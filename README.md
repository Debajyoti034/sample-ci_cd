# Sample CI_CD



Documents of demo-project-python kindly read the instruction and details of the project
In this project we are using python language
We follow the steps for CI : build, source code analysis with sonarqube and publishing the code into jfrog artifactory

job stages:
    - compile
    - test and code_analysis with sonar Qube
    - build and package the codebase into a snapshot jar
    - deploy the jar into an artifactory(Jfrog cloud)

stage 1:
        stage: compile --> It is going to copile the code if it pass then the next job will be triggered.
  tags:
    - we are ussing a registered gitlab runner for it

  script:
    - we are using maven as a build tool for this java spring boot project.

About Sonarqube:-
        1. Bug: A coding error that will break your code.​
        2. Vulnerability: A point in your code that's open to attack.​
        3. Code Smell: A maintainability issue that makes your code confusing and difficult to maintain.​

To setup sonarqube:- we have to follow 4 steps for that and refer the Sonarqube-setup.pdf document in the repo to setup sonarqube step by step..
step1: Set your project key
* Create a sonar-project.properties --> in that file create the content like below mention
sonar.projectKey=[project-name]
sonar.qualitygate.wait=true
step2: Add environment variables
a.) Define the SonarQube Token environment variable
b.)In GitLab, go to Settings > CI/CD > Variables to add the following variable and make sure it is available for your project:
c.)In the Key field, enter SONAR_TOKEN 
d.)In the Value field, enter an existing token, or a newly generated one: Generate a token
e.)Uncheck the "Protect Variable" checkbox
f.)Check the "Mask Variable" checkbox
                    Again set a variable for the URL
a.). Define the SonarQube URL environment variable
Still in Settings > CI/CD > Variables add a new variable and make sure it is available for your project:
b.)In the Key field, enter SONAR_HOST_URL 
c.)In the Value field, enter https://sonarcloud.io/summary/new_code?id=Debajyoti034_sample-ci_cd&branch=main
d.)Uncheck the "Protect Variable" checkbox
e.)Leave the "Mask variable" checkbox unchecked

step3: Create or update the configuration file
Create or update your .gitlab-ci.yml file with the following content their where mention in the sonarqube site

stage 2: "https://sonarcloud.io/summary" kindly refer the document to setup sonaqube in the repo SonarQube-Setup.pdf
        stage: code_analysis--> the stage contain to run is their is any error on the code
        we are using sonar cloud for code analysis tool

![image-2.png](./image-2.png)

![image-1.png](./image-1.png)


